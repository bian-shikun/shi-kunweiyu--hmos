package com.zhushenwudi.whalenowings.component.listcomponent;

import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;

import java.util.HashMap;
import java.util.Map;

/**
 * 评论视图持有者
 */
public class CommentViewHolder {
    // 转换视图
    Component convertView;
    // 缓存组件
    private final Map<Integer, Component> mViews = new HashMap<>(0);

    /**
     * 私有构造方法
     * @param convertView
     */
    private CommentViewHolder(Component convertView) {
        this.convertView = convertView;
        convertView.setTag(this);
    }

    /**
     * 使用单例模式避免多个静态类
     * @param context
     * @param component
     * @param resource
     * @return
     */
    static CommentViewHolder getCommentViewHolder(Context context, Component component, int resource) {
        // 判断参数组件是否为空
        if (component == null) {
            // 为空时,根据参数resource获取组件
            Component createConvertView = LayoutScatter.getInstance(context).parse(resource, null, false);
            // 返回获取到的组件
            return new CommentViewHolder(createConvertView);
        } else {
            // 直接返回组件
            return (CommentViewHolder) component.getTag();
        }
    }

    /**
     * 根据泛型指定视图的类型
     * @param resId
     * @param type
     * @param <T>
     * @return
     */
    private <T extends Component> T getView(int resId, Class<T> type) {
        // 从缓存组件获取视图
        Component view = mViews.get(resId);
        if (view == null) {
            // 在转换视图查找
            view = convertView.findComponentById(resId);
            // 缓存视图
            mViews.put(resId, view);
        }
        return (T) view;
    }

    /**
     * 为外部系统提供的文本通用视图
     * @param resId
     * @return
     */
    public Text getTextView(int resId) {
        return getView(resId, Text.class);
    }

    /**
     * 为外部系统提供的图片通用视图
     * @param resId
     * @return
     */
    public Image getImageView(int resId) {
        return getView(resId, Image.class);
    }
}

package com.zhushenwudi.whalenowings.utils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 用于将绘制的多个点转成字符串
 */
public class GsonUtil {
    private static Gson gson;

    static {
        gson = new Gson();
    }

    private GsonUtil() {}

    /**
     * 将对象转换为 JSON 字符串
     * @param object
     * @return
     */
    public static String objectToString(Object object) {
        String jsonString = null;
        if (gson != null) {
            jsonString = gson.toJson(object);
        }
        return jsonString;
    }

    /**
     * 将 gsonString 转换为通用 Bean
     * @param jsonString
     * @param cls
     * @param <T>
     * @return
     */
    public static <T> T jsonToBean(String jsonString, Class<T> cls) {
        T var1 = null;
        if (gson != null) {
            var1 = gson.fromJson(jsonString, cls);
        }
        return var1;
    }

    /**
     * 转换为列表以解决一般问题
     * @param json
     * @param cls
     * @param <T>
     * @return
     */
    public static <T> List<T> jsonToList(String json, Class<T> cls) {
        List<T> lists = new ArrayList<>(0);
        JsonArray array = new JsonParser().parse(json).getAsJsonArray();
        for (final JsonElement elem : array) {
            lists.add(gson.fromJson(elem, cls));
        }
        return lists;
    }

    /**
     * 转换为带有Map的列表
     * @param gsonString
     * @param <T>
     * @return
     */
    public static <T> List<Map<String, T>> jsonToListMap(String gsonString) {
        List<Map<String, T>> list = null;
        if (gson != null) {
            list = gson.fromJson(gsonString, new TypeToken<List<Map<String, T>>>(){}.getType());
        }
        return list;
    }

    /**
     * 转换为Map
     * @param gsonString
     * @param <T>
     * @return
     */
    public static <T> Map<String, T> jsonToMaps(String gsonString) {
        Map<String, T> map = null;
        if (gson != null) {
            map = gson.fromJson(gsonString, new TypeToken<Map<String, T>>(){}.getType());
        }
        return map;
    }
}

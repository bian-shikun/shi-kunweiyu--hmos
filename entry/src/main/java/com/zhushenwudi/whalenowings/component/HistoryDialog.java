package com.zhushenwudi.whalenowings.component;


import com.zhushenwudi.whalenowings.ResourceTable;
import com.zhushenwudi.whalenowings.utils.DistributedFileUtil;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_CONTENT;
import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_PARENT;

/**
 * 展示设备列表对话框, 用户选择设备(可以是多个设备)进行连接
 */
public class HistoryDialog extends CommonDialog {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    // 角半径
    private static final int CORNER_RADIUS = 10;
    // 上下文
    private final Context context;
    private final DistributedFileUtil fileUtil;
    // 设备信息列表
    private final List<String> listData = new ArrayList<>(0);
//    // 设备Id列表
//    private final List<String> checkedDeviceIds = new ArrayList<>(0);
    // 自定义单击监听接口
    private OnConfirmListener onConfirmListener;
    private OnDeleteListener onDeleteListener;
//    // 确认文本
//    private Text operateConfirm;
    // 取消文本
    private Image btnClose;

    // 容器
    private TableLayout container;
//    // 列表组件适配器
//    private ListComponentAdapter listComponentAdapter;

    /**
     * 构造方法
     * @param context
     */
    public HistoryDialog(Context context, DistributedFileUtil fileUtil) {
        super(context);
        this.context = context;
        this.fileUtil = fileUtil;
    }

    /**
     * 自定义单击监听接口
     */
    public interface OnConfirmListener {
        void onConfirmClick(String selectFileName);
    }
    public interface OnDeleteListener {
        void onDeleteClick(String selectFileName);
    }
    /**
     * 初始值单击监听
     * @param listener
     */
    public void setConfirmListener(OnConfirmListener listener) {
        this.onConfirmListener = listener;
    }
    public void setDeleteListener(OnDeleteListener listener) {
        this.onDeleteListener = listener;
    }

    /**
     * 创建对话框
     */
    @Override
    protected void onCreate() {
        super.onCreate();
        // 初始化界面
        initView();
        // 设置适配器
        setAdapter();
    }

    private void initView() {
        // 获取对话框设备布局组件
        Component rootView = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_dialog_layout_history, null, false);
        // 判断是否找到列表容器
        if (rootView.findComponentById(ResourceTable.Id_container) instanceof TableLayout) {
            // 获取列表容器
            container = (TableLayout) rootView.findComponentById(ResourceTable.Id_container);
        }
        // 判断是否找到取消Icon
        if (rootView.findComponentById(ResourceTable.Id_btn_close) instanceof Image) {
            // 获取取消Icon组件
            btnClose = (Image) rootView.findComponentById(ResourceTable.Id_btn_close);
        }

        // 设置对话框大小
        setSize(MATCH_PARENT, MATCH_CONTENT);
        // 设置对齐
        setAlignment(LayoutAlignment.BOTTOM);
        // 设置圆角
        setCornerRadius(CORNER_RADIUS);
        // 自动关闭
        setAutoClosable(true);
        // 内容组件
        setContentCustomComponent(rootView);
        // 透明度
        setTransparent(true);
        // 组件里的事件
        componentBonding();
    }

    /**
     * 选择与取消事件
     */
    private void componentBonding() {
        // 取消事件
        btnClose.setClickedListener(component -> hide());

        if (onConfirmListener != null) {
            onConfirmListener.onConfirmClick("");
        }
        if (onDeleteListener != null) {
            onDeleteListener.onDeleteClick("");
        }
    }

    /**
     * 设置适配器
     */
    public void setAdapter() {
        getListData();

        container.removeAllComponents();
//        int index =1;
        for (String fileName : listData) {
            Component rootView = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_dialog_history_item, null, false);
            PixelMap pixelMap = fileUtil.readImage(fileName);
            Image itemImg = (Image) rootView.findComponentById(ResourceTable.Id_history_item_picture);
            itemImg.setPixelMap(pixelMap);
            itemImg.setClickedListener(component -> {
                // 回调选择的图片名
                onConfirmListener.onConfirmClick(fileName);
            });
            String titleAndTimeStr = fileName.substring(0, fileName.indexOf("."));
            String[] titleAndTimeArr = titleAndTimeStr.split("_");
            Text title = (Text) rootView.findComponentById(ResourceTable.Id_history_item_title);
            title.setText(titleAndTimeArr[1]);
//            title.setText("研讨手稿"+index);
            Text date = (Text) rootView.findComponentById(ResourceTable.Id_history_item_date);
//            String str = fileName.substring(0, fileName.indexOf("."));
//            long timestamp = Long.parseLong(str);
            long timestamp = Long.parseLong(titleAndTimeArr[0]);
            date.setText(sdf.format(new Date(timestamp)));

            Text delHistory = (Text) rootView.findComponentById(ResourceTable.Id_delHistory);
            delHistory.setClickedListener(component -> {
                onDeleteListener.onDeleteClick(fileName);
            });

//            index++;
            container.addComponent(rootView);
        }


//        // 初始化列表组件适配器
//        listComponentAdapter = new ListComponentAdapter<DeviceData>(context, deviceList, ResourceTable.Layout_dialog_device_item) {
//            @Override
//            public void onBindViewHolder(CommentViewHolder commentViewHolder, DeviceData item, int position) {
//                // 设置列表项文本内容
//                commentViewHolder.getTextView(ResourceTable.Id_item_desc).setText(item.getDeviceInfo().getDeviceName());
//
//                // 根据设备类型设置图标
//                switch (item.getDeviceInfo().getDeviceType()) {
//                    case SMART_PHONE:
//                        commentViewHolder.getImageView(ResourceTable.Id_item_type).setPixelMap(ResourceTable.Media_dv_phone);
//                        break;
//                    case SMART_PAD:
//                        commentViewHolder.getImageView(ResourceTable.Id_item_type).setPixelMap(ResourceTable.Media_dv_pad);
//                        break;
//                    case SMART_WATCH:
//                        commentViewHolder.getImageView(ResourceTable.Id_item_type).setPixelMap(ResourceTable.Media_dv_watch);
//                        break;
//                    default:
//                        break;
//                }
//
//                // 设置是否选择图标
//                commentViewHolder.getImageView(ResourceTable.Id_item_check).setPixelMap(item.isChecked() ? ResourceTable.Media_checked_point : ResourceTable.Media_uncheck_point);
//            }
//
//            @Override
//            public void onItemClick(Component component, DeviceData item, int position) {
//                super.onItemClick(component, item, position);
//                // 是否选择图标
//                deviceList.get(position).setChecked(!item.isChecked());
//                // 通知数据有更改
//                listComponentAdapter.notifyDataChanged();
//            }
//        };
        // 设置列表容器适配器
//        listContainer.setItemProvider(listComponentAdapter);
    }

    private void getListData(){
        // 清空旧数据
        listData.clear();
        // 添加分布式文件列表
        listData.addAll(fileUtil.getFileList());
//        // 判断列表数据
//        if(listData.isEmpty()){
//            // 隐藏列表
//            lcList.setVisibility(Component.HIDE);
//            // 显示无数据提示信息
//            textEmptyHint.setVisibility(Component.VISIBLE);
//        }else {
//            // 显示列表
//            lcList.setVisibility(Component.VISIBLE);
//            // 隐藏无数据提示信息
//            textEmptyHint.setVisibility(Component.HIDE);
//        }
//        // 通知数据更新
//        provider.notifyDataChanged();
    }
}

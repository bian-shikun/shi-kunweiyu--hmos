package com.zhushenwudi.whalenowings.utils;

import com.zhushenwudi.whalenowings.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

/**
 * Toast工具类
 *
 */
public class ToastUtil {
    private ToastDialog toastDialog;

    private ToastUtil() {
    }

    /**
     * 实例
     *
     * @return 实例
     */
    public static ToastUtil getInstance() {
        return ToastUtilInstance.INSTANCE;
    }

    /**
     * Toast实例内部类
     */
    private static class ToastUtilInstance {
        private static final ToastUtil INSTANCE = new ToastUtil();
    }

    /**
     * 显示Toast
     *
     * @param context
     * @param content
     */
    public void showToast(Context context, String content) {
        if (toastDialog != null && toastDialog.isShowing()) {
            toastDialog.cancel();
        }

        Component toastLayout = LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_layout_toast, null, false);
        Text toastText = (Text) toastLayout.findComponentById(ResourceTable.Id_text_msg_toast);
        toastText.setText(content);
        toastDialog = new ToastDialog(context);
        toastDialog.setComponent(toastLayout);
        toastDialog.setTransparent(true);
        toastDialog.show();
    }
}

import featureAbility from '@ohos.ability.featureAbility';

export default {
    data: {
        name: "石鲲未羽",
        isDialogShowing: false,
        projectList: [
            {
                name: "工程名称1", date: "2021-12-12 12:00"
            },
            {
                name: "工程名称2", date: "2021-12-12 12:00"
            },
            {
                name: "工程名称3", date: "2021-12-12 12:00"
            },
            {
                name: "工程名称4", date: "2021-12-12 12:00"
            },
            {
                name: "工程名称5", date: "2021-12-12 12:00"
            },
            {
                name: "add"
            },
        ],
        pdfList: [
            {
                name: "研讨手稿1", date: "2021-12-12 12:00"
            },
            {
                name: "研讨手稿2", date: "2021-12-12 12:00"
            },
            {
                name: "研讨手稿3", date: "2021-12-12 12:00"
            },
            {
                name: "研讨手稿4", date: "2021-12-12 12:00"
            },
            {
                name: "add"
            },
        ],
        modemList: [
            {
                name: "模型外观1", date: "2021-12-12 12:00"
            },
            {
                name: "模型外观2", date: "2021-12-12 12:00"
            },
            {
                name: "模型外观3", date: "2021-12-12 12:00"
            },
            {
                name: "模型外观4", date: "2021-12-12 12:00"
            },
            {
                name: "模型外观5", date: "2021-12-12 12:00"
            },
            {
                name: "add"
            },
        ],
        programList: [
            {
                name: "模型外观1", date: "2021-12-12 12:00"
            },
            {
                name: "模型外观2", date: "2021-12-12 12:00"
            },
            {
                name: "模型外观3", date: "2021-12-12 12:00"
            },
            {
                name: "模型外观4", date: "2021-12-12 12:00"
            },
            {
                name: "add"
            },
        ],
        uxList: [
            {
                name: "上位交互1", date: "2021-12-12 12:00"
            },
            {
                name: "上位交互2", date: "2021-12-12 12:00"
            },
            {
                name: "上位交互3", date: "2021-12-12 12:00"
            },
            {
                name: "上位交互4", date: "2021-12-12 12:00"
            },
            {
                name: "add"
            },
        ]
    },

    switchTitle() {

    },

    openDialog() {
        this.isDialogShowing = true;
    },

    selectBoard() {
        this.isDialogShowing = false;
        var str = {
            "want": {
                "deviceId": "",
                "bundleName": "com.release.whalenowings",
                "abilityName": "com.zhushenwudi.whalenowings.ICBAbility",
                "uri": "",
                "type": "*",
                "options": {},
                "action": "",
                "parameters": {}
            },
            "abilityStartSetting": {}
        };
        featureAbility.startAbility(str)
            .then((data) => {
                console.info('操作成功. Data: ' + JSON.stringify(data))
            }).catch((error) => {
            console.error('操作失败. Cause: ' + JSON.stringify(error));
        })
    },

    selectRun() {
        this.isDialogShowing = false;
    },

    selectInteractive() {
        this.isDialogShowing = false;
        let filepath = "/storage/emulated/0/Android/data/com.zhushenwudi.whalenowings/files/Download/main.xe";
        var str = {
            "want": {
                "deviceId": "",
                "bundleName": "com.release.whalenowings",
                "abilityName": "com.zhushenwudi.whalenowings.MainAbility",
                "uri": filepath,
                "type": "*",
                "options": {},
                "action": "",
                "parameters": {}
            },
            "abilityStartSetting": {}
        };
        featureAbility.startAbility(str)
        .then((data) => {
            console.info('操作成功. Data: ' + JSON.stringify(data))
        }).catch((error) => {
            console.error('操作失败. Cause: ' + JSON.stringify(error));
        })
    }
}
package com.zhushenwudi.whalenowings.bean;

import ohos.distributedschedule.interwork.DeviceInfo;

/**
 * 设备适配器实体类
 */
public class DeviceData {
    // 标识是否选择
    private boolean isChecked;
    // 设备信息对象
    private DeviceInfo deviceInfo;

    /**
     * 构造方法
     * @param isChecked
     * @param deviceInfo
     */
    public DeviceData(boolean isChecked, DeviceInfo deviceInfo) {
        this.isChecked = isChecked;
        this.deviceInfo = deviceInfo;
    }

    // 下面是isChecked, deviceInfo 的Get与Set方法

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }
}

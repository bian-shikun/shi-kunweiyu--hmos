package com.zhushenwudi.whalenowings.slice;

import ohos.agp.components.webengine.BrowserAgent;
import ohos.agp.components.webengine.WebView;
import ohos.app.Context;

public class OBBrowserAgent extends BrowserAgent {
    private final WebView webView;

    public OBBrowserAgent(Context context, WebView webView) {
        super(context);
        this.webView = webView;
    }
}

package com.zhushenwudi.whalenowings.slice;

import ohos.agp.components.webengine.ResourceRequest;
import ohos.agp.components.webengine.WebAgent;
import ohos.agp.components.webengine.WebView;

public class OBWebAgent extends WebAgent {
    private final WebView webView;

    public OBWebAgent(WebView webView) {
        this.webView = webView;
    }

    @Override
    public boolean isNeedLoadUrl(WebView webView, ResourceRequest request) {
        return true;
    }
}

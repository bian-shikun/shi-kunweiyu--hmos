package com.zhushenwudi.whalenowings.utils;

import com.zhushenwudi.whalenowings.ResourceTable;
import com.zhushenwudi.whalenowings.bean.MyPoint;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Texture;
import ohos.agp.utils.Point;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImagePacker;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 分布式文件工具类
 */
public class DistributedFileUtil {
    // 上下文
    private final Context mContext;

    /**
     * 构造方法
     * @param context
     */
    public DistributedFileUtil(Context context) {
        this.mContext = context;
    }

    /**
     * 保存图片
     * @param fileName
     * @return
     */
    public PixelMap writeLetter(String fileName, List<MyPoint> points, String bgImage) {
        // 获取分布式文件路径
        String filePath = mContext.getDistributedDir() + File.separator + fileName + ".jpg";
        Texture texture=null;
        try {
            // 从资源文件获取背景图片
            int bgImg = -1;
            ImageSource imageSource;
            ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
            srcOpts.formatHint = "image/jpeg";
            if (bgImage.equalsIgnoreCase("history") || bgImage.equalsIgnoreCase("lately")) {
                imageSource = ImageSource.create(filePath, srcOpts);
            }else{
                if (bgImage.equalsIgnoreCase("white")) {
                    bgImg = ResourceTable.Media_bg_white;
                }else if (bgImage.equalsIgnoreCase("eye")) {
                    bgImg = ResourceTable.Media_bg_eye;
                }
                InputStream inputStream = mContext.getResourceManager().getResource(bgImg);
                imageSource = ImageSource.create(inputStream, srcOpts);
            }


            // 设置图片参数
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            decodingOptions.desiredSize=new Size(2560,1600);
//            decodingOptions.desiredSize=new Size(1280,800);
            PixelMap pixelMap = imageSource.createPixelmap(decodingOptions);
            //用于保存画图结果
            texture=new Texture(pixelMap);

            Canvas canvas=new Canvas(texture);
            draw(points, canvas);

            // 文件输出流
            FileOutputStream fos=new FileOutputStream(filePath);

            ImagePacker imagePacker = ImagePacker.create();
            ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
            packingOptions.format = "image/jpeg";//只支持image/jpeg
            packingOptions.quality = 100;
            boolean result = imagePacker.initializePacking(fos, packingOptions);
            if(result)
            {
                //这里获取绘画后的pixelMap用来保存
                result = imagePacker.addImage(texture.getPixelMap());
                if (result) {
                    long dataSize = imagePacker.finalizePacking();
                    System.out.println("文件大小："+dataSize);
                    ToastUtil.getInstance().showToast(mContext, "保存成功！");
                }
            }

            fos.flush();
            fos.close();
        } catch (IOException | NotExistException e) {
            System.out.println("文件保存出错："+e.getMessage());
            e.printStackTrace();
        }

        return texture.getPixelMap();
    }

    /**
     * 读取图片
     * @param fileName
     * @return
     */
    public PixelMap readImage(String fileName) {
        // 获取分布式文件路径
        String filePath = mContext.getDistributedDir() + File.separator + fileName;
        // 根据分布式文件路径,生成文件
        File file = new File(filePath);
        if (!file.exists()) {
            // 如果文件不存在, 调用写信件
            writeLetter(fileName, new ArrayList<>(), "white");
        }
        // 图片参数
        ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
        srcOpts.formatHint = "image/jpeg";
        // 创建图片源
        ImageSource imageSource = ImageSource.create(file, srcOpts);
        // 生成图片
        PixelMap pixelMap = imageSource.createPixelmap(null);

        return pixelMap;
    }

    public boolean deleteImage(String fileName) {
        try{
            // 获取分布式文件路径
            String filePath = mContext.getDistributedDir() + File.separator + fileName;
            // 根据分布式文件路径,生成文件
            File file = new File(filePath);
            if (file.exists()) {
                return file.delete();
            }

            return true;
        }catch (Exception e) {
            return false;
        }
    }

    /**
     * 获取文件列表
     * @return
     */
    public List<String> getFileList() {
        // 获取分布式文件列表
        File[] files = mContext.getDistributedDir().listFiles();
        List<File> listFile = new ArrayList<>(Arrays.asList(files));
        // 排序文件顺序
        listFile.sort((file, newFile) -> {
            if (file.lastModified() > newFile.lastModified()) {
                return -1;
            } else if (file.lastModified() == newFile.lastModified()) {
                return 0;
            } else {
                return 1;
            }
        });
        List<String> listFileName = new ArrayList<>();
        // 获取文件列表文件名
        for (File f : listFile) {
            if (f.isFile()) {
                String name = f.getName();
                listFileName.add(name);
            }
        }
        return listFileName;
    }

    /**
     * 把所有点集合,画到画布下
     * @param myPoints
     * @param canvas
     */
    private void draw(List<MyPoint> myPoints, Canvas canvas) {
        if (myPoints == null || myPoints.size() <= 1) {
            return;
        }
        Paint paint = new Paint();
        // 设置 Paint 对象的抗锯齿。
        paint.setAntiAlias(true);
        // 设置 Paint 对象的样式属性(指示笔画类型，具有此样式的几何体将被描边。)
        paint.setStyle(Paint.Style.STROKE_STYLE);

        // 第一笔为最近一笔
        Point first = null;
        // 最后一笔
        Point last = null;

        for (MyPoint myPoint : myPoints) {
            // 设置画笔颜色
            paint.setColor(myPoint.getPaintColor());
            // 设置画笔粗细
            paint.setStrokeWidth(myPoint.getPaintSize());
            // 设置透明度
            paint.setAlpha(myPoint.getPaintAlpha());
            // 获取X坐标值
            float finalX = myPoint.getPositionX();
            // 获取Y坐标值
            float finalY = myPoint.getPositionY();
            // 初始化点
            Point finalPoint = new Point(finalX, finalY);
            // 如果是最后一点, 跳过
            if (myPoint.isLastPoint()) {
                first = null;
                last = null;
                continue;
            }
            // 记录第一笔
            if (first == null) {
                first = finalPoint;
            } else {
                // 如果不是最后一笔
                if (last != null) {
                    // 第一笔为上一笔
                    first = last;
                }
                // 更新最近一笔为最后一笔
                last = finalPoint;
                // 把点画到画布上
                canvas.drawLine(first, last, paint);
            }
        }
    }
}

package com.zhushenwudi.whalenowings;

import ohos.aafwk.ability.IAbilityContinuation;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.ace.ability.AceAbility;
import com.zhushenwudi.whalenowings.slice.*;

public class Runtime extends AceAbility implements IAbilityContinuation{
    @Override
    public void onStart(Intent intent) {
//        setInstanceName("default");
        setInstanceName("openblockhos");
        super.onStart(intent);
        super.setMainRoute(RuntimeSlice.class.getName());
//        requestPermissionsFromUser(new String[]{"ohos.permission.DISTRIBUTED_DATASYNC"}, 0);
    }

    @Override
    public boolean onStartContinuation() {
        return true;
    }

    @Override
    public boolean onSaveData(IntentParams intentParams) {
        return true;
    }

    @Override
    public boolean onRestoreData(IntentParams intentParams) {
        return true;
    }

    @Override
    public void onCompleteContinuation(int i) {

    }
}

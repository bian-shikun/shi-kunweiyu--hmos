package com.zhushenwudi.whalenowings.bean;

import ohos.agp.utils.Color;
import ohos.agp.utils.Point;

/**
 * 存放绘制点的坐标和基本信息
 */
public class MyPoint extends Point {
    // X坐标
    private final float positionX;
    // Y坐标
    private final float positionY;
    // 是否最后一点
    private boolean isLastPoint = false;
    // 颜色
    private Color paintColor;
    // 粗细
    private int paintSize;
    // 透明度
    private float paintAlpha;

    public MyPoint(float x, float y) {
        super();
        this.positionX = x;
        this.positionY = y;
    }

    public float getPositionX() {
        return positionX;
    }

    public float getPositionY() {
        return positionY;
    }

    public boolean isLastPoint() {
        return isLastPoint;
    }

    public void setLastPoint(boolean lastPoint) {
        isLastPoint = lastPoint;
    }

    public Color getPaintColor() {
        return paintColor;
    }

    public void setPaintColor(Color paintColor) {
        this.paintColor = paintColor;
    }

    public int getPaintSize() {
        return paintSize;
    }

    public void setPaintSize(int paintSize) {
        this.paintSize = paintSize;
    }

    public float getPaintAlpha() {
        return paintAlpha;
    }

    public void setPaintAlpha(float paintAlpha) {
        this.paintAlpha = paintAlpha;
    }
}

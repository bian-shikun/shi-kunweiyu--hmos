package com.zhushenwudi.whalenowings;

import com.zhushenwudi.whalenowings.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.ace.ability.AceAbility;
import ohos.bundle.IBundleManager;

import static ohos.security.SystemPermission.*;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
//        requestPermissionsFromUser(new String[]{"ohos.permission.DISTRIBUTED_DATASYNC"}, 0);
        requestPermission();
    }
    private void requestPermission() {
        // 判断是否已经授权
        if (verifySelfPermission(DISTRIBUTED_DATASYNC) != IBundleManager.PERMISSION_GRANTED) {
            // 如果还没有授权, 弹窗出来让用户选择
            if (canRequestPermission(DISTRIBUTED_DATASYNC)) {
                requestPermissionsFromUser(new String[]{DISTRIBUTED_DATASYNC,WRITE_MEDIA,READ_MEDIA}, 0);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}

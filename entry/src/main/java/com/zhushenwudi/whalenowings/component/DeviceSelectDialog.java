package com.zhushenwudi.whalenowings.component;

import com.zhushenwudi.whalenowings.ResourceTable;
import com.zhushenwudi.whalenowings.bean.DeviceData;
import com.zhushenwudi.whalenowings.component.listcomponent.CommentViewHolder;
import com.zhushenwudi.whalenowings.component.listcomponent.ListComponentAdapter;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;
import ohos.distributedschedule.interwork.DeviceInfo;
import ohos.distributedschedule.interwork.DeviceManager;

import java.util.ArrayList;
import java.util.List;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_CONTENT;
import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_PARENT;

/**
 * 展示设备列表对话框, 用户选择设备(可以是多个设备)进行连接
 */
public class DeviceSelectDialog extends CommonDialog {
    // 角半径
    private static final int CORNER_RADIUS = 10;
    // 上下文
    private final Context context;
    // 设备信息列表
    private final List<DeviceData> deviceList = new ArrayList<>(0);
    // 设备Id列表
    private final List<String> checkedDeviceIds = new ArrayList<>(0);
    // 自定义单击监听接口
    private OnclickListener onclickListener;
    // 确认文本
    private Text operateConfirm;
    // 取消文本
    private Text operateCancel;

    // 列表容器
    private ListContainer listContainer;
    // 列表组件适配器
    private ListComponentAdapter listComponentAdapter;

    /**
     * 构造方法
     * @param context
     */
    public DeviceSelectDialog(Context context) {
        super(context);
        this.context = context;
    }

    /**
     * 自定义单击监听接口
     */
    public interface  OnclickListener {
        void onConfirmClick(List<String> deviceIds);
    }
    /**
     * 初始值单击监听
     * @param listener
     */
    public void setListener(OnclickListener listener) {
        this.onclickListener = listener;
    }

    /**
     * 创建对话框
     */
    @Override
    protected void onCreate() {
        super.onCreate();
        // 初始化界面
        initView();
        // 设置适配器
        setAdapter();
    }

    private void initView() {
        // 获取对话框设备布局组件
        Component rootView = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_dialog_layout_device, null, false);
        // 判断是否找到列表容器
        if (rootView.findComponentById(ResourceTable.Id_list_container_device) instanceof ListContainer) {
            // 获取列表容器
            listContainer = (ListContainer) rootView.findComponentById(ResourceTable.Id_list_container_device);
        }
        // 判断是否找到确定文本
        if (rootView.findComponentById(ResourceTable.Id_operate_yes) instanceof Text) {
            // 获取确定文本组件
            operateConfirm = (Text) rootView.findComponentById(ResourceTable.Id_operate_yes);
        }
        // 判断是否找到取消文本
        if (rootView.findComponentById(ResourceTable.Id_operate_no) instanceof Text) {
            // 获取取消文本组件
            operateCancel = (Text) rootView.findComponentById(ResourceTable.Id_operate_no);
        }

        // 设置对话框大小
        setSize(MATCH_PARENT, MATCH_CONTENT);
        // 设置对齐
        setAlignment(LayoutAlignment.BOTTOM);
        // 设置圆角
        setCornerRadius(CORNER_RADIUS);
        // 自动关闭
        setAutoClosable(true);
        // 内容组件
        setContentCustomComponent(rootView);
        // 透明度
        setTransparent(true);
        // 组件里的事件
        componentBonding();
    }

    /**
     * 确定与取消事件
     */
    private void componentBonding() {
        // 确定事件
        operateConfirm.setClickedListener(component -> {
            if (onclickListener != null) {
                // 清空设备Id列表数据
                checkedDeviceIds.clear();
                // 封装设备
                cirDevice();
            }
        });
        // 取消事件
        operateCancel.setClickedListener(component -> hide());
    }

    /**
     * 封装设备信息
     */
    private void cirDevice() {
        // 遍历设备数据
        for (DeviceData deviceData : deviceList) {
            // 筛选出已经选择了的设备
            if (deviceData.isChecked()) {
                // 把选择设备存储到checkDeviceIds列表
                checkedDeviceIds.add(deviceData.getDeviceInfo().getDeviceId());
            }
        }
        // 调用单击接口的确定点击方法
        onclickListener.onConfirmClick(checkedDeviceIds);
    }

    /**
     * 设置适配器
     */
    private void setAdapter() {
        // 获取在线设备信息
        List<DeviceInfo> deviceInfoList = DeviceManager.getDeviceList(DeviceInfo.FLAG_GET_ONLINE_DEVICE);
        // 清空设备列表数据
        deviceList.clear();
        // 封装设备信息到deviceList列表
        for (DeviceInfo deviceInfo : deviceInfoList) {
            deviceList.add(new DeviceData(false, deviceInfo));
        }
        // 初始化列表组件适配器
        listComponentAdapter = new ListComponentAdapter<DeviceData>(context, deviceList, ResourceTable.Layout_dialog_device_item) {
            @Override
            public void onBindViewHolder(CommentViewHolder commentViewHolder, DeviceData item, int position) {
                // 设置列表项文本内容
                commentViewHolder.getTextView(ResourceTable.Id_item_desc).setText(item.getDeviceInfo().getDeviceName());

                // 根据设备类型设置图标
                switch (item.getDeviceInfo().getDeviceType()) {
                    case SMART_PHONE:
                        commentViewHolder.getImageView(ResourceTable.Id_item_type).setPixelMap(ResourceTable.Media_dv_phone);
                        break;
                    case SMART_PAD:
                        commentViewHolder.getImageView(ResourceTable.Id_item_type).setPixelMap(ResourceTable.Media_dv_pad);
                        break;
                    case SMART_WATCH:
                        commentViewHolder.getImageView(ResourceTable.Id_item_type).setPixelMap(ResourceTable.Media_dv_watch);
                        break;
                    default:
                        break;
                }

                // 设置是否选择图标
                commentViewHolder.getImageView(ResourceTable.Id_item_check).setPixelMap(item.isChecked() ? ResourceTable.Media_checked_point : ResourceTable.Media_uncheck_point);
            }

            @Override
            public void onItemClick(Component component, DeviceData item, int position) {
                super.onItemClick(component, item, position);
                // 是否选择图标
                deviceList.get(position).setChecked(!item.isChecked());
                // 通知数据有更改
                listComponentAdapter.notifyDataChanged();
            }
        };
        // 设置列表容器适配器
        listContainer.setItemProvider(listComponentAdapter);
    }
}
